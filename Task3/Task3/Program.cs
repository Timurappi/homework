﻿using System;

namespace Circle
{
    class Program
    {
        static void Main(string[] args)
        {
            double r;
            double Pi = 3.14;

            Console.WriteLine("Enter the radius");
            r = double.Parse(Console.ReadLine());

            double area = Pi * r * r;

            Console.WriteLine("The area of a circle is " + area);
        }
    }
}
