﻿using System;


namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b, c;
            Console.WriteLine("Enter number 1");
            a = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter number 2");
            b = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter number 3");
            c = double.Parse(Console.ReadLine());

            double arithm = (a + b + c) / 3;

            Console.WriteLine("Average arithmetic mean is " + arithm);
        }
    }
}
