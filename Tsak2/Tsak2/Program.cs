﻿using System;


namespace Tsak2
{
    class Program
    {
        static void Main(string[] args)
        {
            double length, width;
            Console.WriteLine("Enter length");
            length = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter width");
            width = double.Parse(Console.ReadLine());

            double area = length * width;
            Console.WriteLine("The area is " + area);
        }
    }
}
