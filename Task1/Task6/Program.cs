﻿using System;

namespace Task6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the first number");
            double number1 = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter the second number");
            double number2 = double.Parse(Console.ReadLine());

            double sum = number1 + number2;

            if(sum > 100)
            {
            Console.WriteLine("YES");
            }
            else
            {
            Console.WriteLine("NO");
            }
        }
    }
}
