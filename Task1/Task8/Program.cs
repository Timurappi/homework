﻿using System;

namespace Task8
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = int.Parse(Console.ReadLine());
            switch (a)
            {
                case 1:
                    Console.WriteLine("Entered number 1");
                    break;
                case 2:
                    Console.WriteLine("Entered number 2");
                    break;
                default:
                    Console.WriteLine("Incorrect Number");
                    break;
                  
            }
            
        }
    }
}
