﻿using System;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b;

            Console.WriteLine("Enter first number");
            a = double.Parse(Console.ReadLine());
            
            Console.WriteLine("Enter second number");
            b = double.Parse(Console.ReadLine());

            double sum = a + b;
            double multi = a * b;

            Console.WriteLine("The sum is " + sum);
            Console.WriteLine("The multiplication is " + multi);
        }
    }
}
