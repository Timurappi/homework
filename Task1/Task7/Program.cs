﻿using System;

namespace Task7
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            int b;
            Console.WriteLine("Enter the number");
            a = int.Parse(Console.ReadLine());
            b = a % 2;
            
            if (b == 0) 
            {
                Console.WriteLine("Even Number - YES");
            }
            else
            {
                Console.WriteLine("Odd Number - NO");
            }
        }
    }
}
