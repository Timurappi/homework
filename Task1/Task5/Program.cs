﻿using System;

namespace Task5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the number");
            double a = double.Parse(Console.ReadLine());

            if (a > 10)
            {
                Console.WriteLine("The result is");
                Console.WriteLine(a - 10);
            }
            else
            {
                Console.WriteLine("The result is");
                Console.WriteLine(a + 10);
            }
        }
    }
}
