﻿using System;


//
namespace Lesson
{
    class Program
    {

        static void Main(string[] args)
        {
            double a, b, c;

            Console.WriteLine("Enter first number");
            a = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter math symbol (+-*/)");
            c = char.Parse(Console.ReadLine());

            Console.WriteLine("Enter second number");
            b = double.Parse(Console.ReadLine());


            switch (c)
            {
                case '+':
                    double sum = a + b;
                    Console.WriteLine("The result is " + sum);
                    break;
                case '*':
                    double multi = a * b;
                    Console.WriteLine("The result is " + multi);
                    break;
                case '/':
                    double divide = a / b;
                    Console.WriteLine("The result is " + divide);
                    break;
                case '-':
                    double minus = a - b;
                    Console.WriteLine("The result is " + minus);
                    break;
            }

        }
    }
}
